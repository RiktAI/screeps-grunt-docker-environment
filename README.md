# screeps-grunt-docker-environment

A basic environment for developing your own Screeps AI in your IDE of choice.

It is based on the [Advanced Grunt Article](https://docs.screeps.com/contributed/advanced_grunt.html),
but up-to-date.
_(following the article without modification gave me some errors I had to fix first.)_

Also it provides Autocomplete. 

As a little cherry on the top there is a docker.sh which will provide you a throw-away container for using grunt. 

## Features

 * Autocomplete through [Garethp's ScreepsAutocomplete](https://github.com/Garethp/ScreepsAutocomplete)
    * out-of-the-box for (Web|Php)Storm-user
    * see the linked repo for installation instructions for other IDEs
 * Grunt with several Tasks:
    * concat and upload your code with `grunt` to the official server
        * public test realm is also supported
        * private server are possible, too; but need a password set on the server
    * lint your code with `grunt lint`
    * beautify your code with `grunt beautify`
* Docker
    * you can run any commands above in docker, just **replace** the `grunt` with `bash docker.sh`
    1. the container starts
    2. grunt runs
    3. the container will exit with the exitcode of the grunt-task
    

## Usage

 * Clone the repo
 * copy `.screeps.json.example` to `.screeps.json` and adjust it
 * read the Features section of this Readme, it's also the manual
 * You want to write your own small Game-AI in JS ... you will figure out the rest.
 