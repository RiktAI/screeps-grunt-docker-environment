require('version');

function codeUpdated() {

    if (!Memory.SCRIPT_VERSION || Memory.SCRIPT_VERSION != SCRIPT_VERSION) {
        Memory.SCRIPT_VERSION = SCRIPT_VERSION;
        return true;
    }
    return false;
}

module.exports.loop = function() {

    if (codeUpdated()) {
          console.log('=== Code Updated in Tick ' + Game.time + ' ===');
	    }

	    console.log("Hello Screeper! The time is " + Game.time + " o'Tick. See ya next Tick");

};